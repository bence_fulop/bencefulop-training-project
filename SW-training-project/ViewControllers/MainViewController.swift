//
//  ViewController.swift
//  SW-training-project
//
//  Created by Bence Fulop on 7/5/21.
//

import UIKit

let APITypes: [Int: String] = [
    0: "People" ,
    1: "Planets",
    2: "Starships",
    3: "Vehicles",
    4: "Species",
    5: "Films",
]


class MainViewController: UIViewController {
    var names = ["Person", "Planet", "Starship", "Vehicle", "Species", "Film"]

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var searchBar: UISearchBar! {
        didSet {
            searchBar.delegate = self
        }
    }
    
    @IBAction func homeButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func closeButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }


    @IBAction func segmentedControlTapped(_ sender: Any) {
        placeholderValue = APITypes[segmentedControl.selectedSegmentIndex]
        searchBar.placeholder = "search \(placeholderValue!.lowercased())..."
    }
    
    @IBOutlet weak var searchButton: UIButton!

    private var searchedName: String?
    private var placeholderValue: String?
    private var spinnerViewController = SpinnerViewController()
    private var result: SWObject?
    private var resultList = [SWObject]()
    private let transparentImage = UIImage()
    private var personClient: PersonClientType = PersonClient()
    private var planetClient: PlanetClientType = PlanetClient()
    private var starshipClient: StarshipClientType = StarshipClient()
    private var vehicleClient: VehicleClientType = VehicleClient()
    private var filmClient: FilmClientType = FilmClient()
    private var speciesClient: SpeciesClientType = SpeciesClient()

    @IBAction func searchButtonClicked(_ sender: Any) {
        createSpinnerView(self)
        if let tempSearchedName = searchedName {
            switch APITypes[segmentedControl.selectedSegmentIndex] {
            case "People":
                personClient.getPerson(name: tempSearchedName) { response in
                    self.handleSearchResponse(from: response)
                }
            case "Planets":
                planetClient.getPlanet(name: tempSearchedName) { response in
                    self.handleSearchResponse(from: response)
                }
            case "Starships":
                starshipClient.getStarship(name: tempSearchedName) { response in
                    self.handleSearchResponse(from: response)
                }
            case "Vehicles":
                vehicleClient.getVehicle(name: tempSearchedName) { response in
                    self.handleSearchResponse(from: response)
                }
            case "Species":
                speciesClient.getSpecie(name: tempSearchedName) { response in
                    self.handleSearchResponse(from: response)
                }
            case "Films":
                filmClient.getFilm(name: tempSearchedName) { response in
                    self.handleSearchResponse(from: response)
                }
            default:
                break
            }
        } else {
            searchButton.isEnabled = false
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        searchedName = nil
        searchBar.text = ""
        searchButton.isEnabled = false
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        registerNib()
        segmentedControl.backgroundColor = .white
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .selected)
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        searchBar.backgroundImage = transparentImage
        searchBar.searchTextField.backgroundColor = .white
        searchBar.searchTextField.textColor = .black
        searchButton.setTitleColor(UIColor.gray, for: .disabled)
        searchButton.layer.cornerRadius = 10
        searchButton.clipsToBounds = true
    }

    //MARK: - API Response Handling

    private func handleSearchResponse(from response: SWObject?) {
        if let response = response {
            DispatchQueue.main.async {
                self.result = response
                self.performSegue(withIdentifier: "goToResult", sender: self)
            }
        }
        else {
            let alert = UIAlertController(title: "Oh dear..", message: "A \(annotateAlertType()) called \"\(searchedName!)\" can't be found in this galaxy.", preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "Try again", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }

// response handling for lists
    private func handleListResponse(from response: [SWObject]?) {
        if let response = response {
            self.resultList = response
            self.performSegue(withIdentifier: "goToList", sender: self)
        }
    }

    //MARK: - Navigation to results
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToResult" {
            let destinationVC = segue.destination as! ResultViewController
            destinationVC.result = result
        }
        if segue.identifier == "goToList" {
            let destinationVC = segue.destination as! ListViewController
            destinationVC.resultList = resultList
        }
    }


    private func annotateAlertType() -> String {
        var categoryForAlert: String?
        switch APITypes[segmentedControl.selectedSegmentIndex] {
        case "People":
            categoryForAlert = "person"
        case "Planets":
            categoryForAlert = "planet"
        case "Starships":
            categoryForAlert = "starship"
        case "Vehicles":
            categoryForAlert = "vehicle"
        case "Species":
            categoryForAlert = "species"
        case "Films":
            categoryForAlert = "film"
        default:
            break
        }
    return categoryForAlert!
    }

    func registerNib() {
        let nib = UINib(nibName: CollectionViewCell.nibName, bundle: nil)
        collectionView?.register(nib, forCellWithReuseIdentifier: CollectionViewCell.reuseIdentifier)
        if let flowLayout = self.collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 1, height: 1)
        }
    }
}

//MARK: - SearchBar Delegate

extension MainViewController: UISearchBarDelegate {
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchedName = searchBar.text
    }

    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchButton.isEnabled = false
        return true
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        searchButton.isEnabled = true
    }
}

//MARK: - Collection View Delegate

extension MainViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return names.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.reuseIdentifier, for: indexPath) as? CollectionViewCell {
            let name = APITypes[indexPath.row]!
            cell.configureCell(name: name)
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
}

extension MainViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let cell: CollectionViewCell = Bundle.main.loadNibNamed(CollectionViewCell.nibName,
                                                                      owner: self,
                                                                      options: nil)?.first as? CollectionViewCell else {
            return CGSize.zero
        }
        cell.configureCell(name: names[indexPath.row])
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        let size: CGSize = cell.contentView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        return CGSize(width: size.width, height: 30)
    }
}

extension MainViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let collectionViewLabelText = (collectionView.cellForItem(at: indexPath) as? CollectionViewCell)?.nameLabel.text {
            createSpinnerView(self)
            switch collectionViewLabelText {
            case "People":
                personClient.getPeople(pageNumber: 1, people: [Person]()) { response in
                        self.handleListResponse(from: response)
                    }
            case "Planets":
                planetClient.getPlanets(pageNumber: 1, planets: [Planet]()){ response in
                    self.handleListResponse(from: response)
                }
            case "Starships":
                starshipClient.getStarships(pageNumber: 1, starships: [Starship]()){ response in
                    self.handleListResponse(from: response)
                }
            case "Vehicles":
                vehicleClient.getVehicles(pageNumber: 1, vehicles: [Vehicle]()){ response in
                    self.handleListResponse(from: response)
                }
            case "Species":
                speciesClient.getSpecies(pageNumber: 1, species: [Species]()){ response in
                    self.handleListResponse(from: response)
                }
            case "Films":
                filmClient.getFilms(pageNumber: 1, films: [Film]()){ response in
                    self.handleListResponse(from: response)
                }
            default:
                break
            }
        }
        //        self.performSegue(withIdentifier: "goToList", sender: self)
    }
}
