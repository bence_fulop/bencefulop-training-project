//
//  PlanetClientAPI.swift
//  SW-training-project
//
//  Created by Bence Fulop on 12/5/21.
//

import Foundation

enum PlanetClientAPIType {
    case getPlanets(Int)
    case getPlanet(String)
}

struct PlanetClientAPI: SWAPI {
    let type: PlanetClientAPIType

    var path: String {
        switch type {
        case .getPlanets:
            return "/planets/"
        case .getPlanet:
            return "/planets/"
        }
    }

    var method: RequestMethod {
        switch type {
        case .getPlanets, .getPlanet:
            return .get
        }
    }

    var parameters: RequestParameters? {
        switch type {
        case .getPlanets(let pageNumber):
            return ["page": "\(pageNumber)"]
        case .getPlanet(let name):
            return ["search": name]
        }
    }

    init(type: PlanetClientAPIType) {
        self.type = type
    }
}

