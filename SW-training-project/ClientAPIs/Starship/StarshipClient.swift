//
//  StarshipClient.swift
//  SW-training-project
//
//  Created by Bence Fulop on 12/5/21.
//

import Combine
import Foundation

protocol StarshipClientType {
    func getStarships(pageNumber: Int, starships: [Starship], completionHandler: @escaping ([Starship]?) -> Void)
    func getStarship(name: String, completionHandler: @escaping (Starship?) -> Void)
}

class StarshipClient: StarshipClientType {

    private var subscriptions: Set<AnyCancellable> = []
    private let httpClient: HTTPClientType
    private let base: URL

    init() {
        httpClient = HTTPClient()
        base = URL(string: "https://swapi.dev/api")!
    }

    func getStarships(pageNumber: Int, starships: [Starship], completionHandler: @escaping ([Starship]?) -> Void) {
       (httpClient.requestObject(from: base,
                                 using: StarshipClientAPI(type: .getStarships(pageNumber))) as AnyPublisher<StarshipResponse, SWError>)
           .sink { completion in
               switch completion {
               case .failure:
                   completionHandler(nil)
               case .finished:
                   break
               }
           } receiveValue: { response in
               if response.next != nil {
                   self.getStarships(pageNumber: pageNumber + 1, starships: starships + response.results, completionHandler: completionHandler)
               } else {
                   completionHandler(starships + response.results)
               }
           }
           .store(in: &subscriptions)
   }

    func getStarship(name: String, completionHandler: @escaping (Starship?) -> Void) {
        (httpClient.requestObject(from: base,
                                  using: StarshipClientAPI(type: .getStarship(name))) as AnyPublisher<StarshipResponse, SWError>)
            .sink { completion in
                switch completion {
                case .failure:
                    completionHandler(nil)
                case .finished:
                    break
                }
            } receiveValue: { response in
                completionHandler(response.results.first)
            }
            .store(in: &subscriptions)
    }
}



