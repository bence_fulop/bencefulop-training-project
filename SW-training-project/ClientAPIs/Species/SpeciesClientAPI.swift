//
//  SpeciesClientAPI.swift
//  SW-training-project
//
//  Created by Bence Fulop on 12/5/21.
//

import Foundation

enum SpeciesClientAPIType {
    case getSpecies(Int)
    case getSpecie(String)
}

struct SpeciesClientAPI: SWAPI {
    let type: SpeciesClientAPIType

    var path: String {
        switch type {
        case .getSpecies:
            return "/species/"
        case .getSpecie:
            return "/species/"
        }
    }

    var method: RequestMethod {
        switch type {
        case .getSpecies, .getSpecie:
            return .get
        }
    }

    var parameters: RequestParameters? {
        switch type {
        case .getSpecies(let pageNumber):
            return ["page": "\(pageNumber)"]
        case .getSpecie(let name):
            return ["search": name]
        }
    }

    init(type: SpeciesClientAPIType) {
        self.type = type
    }
}
