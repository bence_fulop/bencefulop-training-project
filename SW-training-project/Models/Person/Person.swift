//
//  Character.swift
//  SW-training-project
//
//  Created by Bence Fulop on 10/5/21.
//

import Foundation

struct Person: SWObject {
    var identifier: String {
        return "PersonTableViewCell"
    }
    var name: String?
    var height: String?
    var mass: String?
    var hair_color: String?
    var skin_color: String?
    var eye_color: String?
    var birth_year: String?
    var gender: String?
}
