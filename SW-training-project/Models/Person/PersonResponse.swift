//
//  CharacterResponse.swift
//  SW-training-project
//
//  Created by Bence Fulop on 10/5/21.
//

import Foundation

struct PersonResponse: Codable {
    let next: String?
    let results: [Person] 
}

