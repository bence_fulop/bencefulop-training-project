//
//  FilmClientAPI.swift
//  SW-training-project
//
//  Created by Bence Fulop on 12/5/21.
//

import Foundation

enum FilmClientAPIType {
    case getFilms(Int)
    case getFilm(String)
}

struct FilmClientAPI: SWAPI {
    let type: FilmClientAPIType

    var path: String {
        switch type {
        case .getFilms:
            return "/films/"
        case .getFilm:
            return "/films/"
        }
    }

    var method: RequestMethod {
        switch type {
        case .getFilms, .getFilm:
            return .get
        }
    }

    var parameters: RequestParameters? {
        switch type {
        case .getFilms(let pageNumber):
            return ["page": "\(pageNumber)"]
        case .getFilm(let name):
            return ["search": name]
        }
    }

    init(type: FilmClientAPIType) {
        self.type = type
    }
}
