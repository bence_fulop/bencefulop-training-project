//
//  Film.swift
//  SW-training-project
//
//  Created by Bence Fulop on 12/5/21.
//

import Foundation

struct Film: SWObject {
    var identifier: String {
        return "FilmTableViewCell"
    }
    var title: String?
    var episode_id: Int?
    var opening_crawl: String?
    var director: String?
    var producer: String?
    var release_date: String?
}
