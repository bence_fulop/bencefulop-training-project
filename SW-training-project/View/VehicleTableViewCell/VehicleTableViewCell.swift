//
//  VehicleTableViewCell.swift
//  SW-training-project
//
//  Created by Bence Fulop on 17/5/21.
//

import UIKit

class VehicleTableViewCell: UITableViewCell, SWTableViewCellType {
    func configure(object: SWObject) {
        guard let object = object as? Vehicle else {
            return
        }
        vehicleNameLabel.text = object.name
        vehicleModelLabel.text = object.model
        vehicleManufacturerLabel.text = object.manufacturer
        vehicleCostInCreditsLabel.text = object.cost_in_credits
        vehicleLengthLabel.text = "\(object.length!) m"
        vehicleCrewLabel.text = object.crew
        vehicleMaxAtmospheringSpeedLabel.text = "\(object.max_atmosphering_speed!) km/h"
        vehicleClassLabel.text = object.vehicle_class
    }
    @IBOutlet var vehicleNameLabel: UILabel!
    @IBOutlet var vehicleModelLabel: UILabel!
    @IBOutlet var vehicleManufacturerLabel: UILabel!
    @IBOutlet var vehicleCostInCreditsLabel: UILabel!
    @IBOutlet var vehicleLengthLabel: UILabel!
    @IBOutlet var vehicleCrewLabel: UILabel!
    @IBOutlet var vehicleMaxAtmospheringSpeedLabel: UILabel!
    @IBOutlet var vehicleClassLabel: UILabel!
}





