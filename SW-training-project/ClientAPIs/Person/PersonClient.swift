//
//  UpClient.swift
//  LearningARQ
//
//  Created by Aurelien Debarge on 5/5/21.
//

import Combine
import Foundation

protocol PersonClientType {
     func getPeople(pageNumber: Int, people: [Person], completionHandler: @escaping ([Person]?) -> Void)
     func getPerson(name: String, completionHandler: @escaping (Person?) -> Void)
}

class PersonClient: PersonClientType {
    
    private var subscriptions: Set<AnyCancellable> = []
    private let httpClient: HTTPClientType
    private let base: URL
    
    init() {
        httpClient = HTTPClient()
        base = URL(string: "https://swapi.dev/api")!
    }
    
     func getPeople(pageNumber: Int, people: [Person], completionHandler: @escaping ([Person]?) -> Void) {
        (httpClient.requestObject(from: base,
                                  using: PersonClientAPI(type: .getPeople(pageNumber))) as AnyPublisher<PersonResponse, SWError>)
            .sink { completion in
                switch completion {
                case .failure:
                    completionHandler(nil)
                case .finished:
                    break
                }
            } receiveValue: { response in
                if response.next != nil {
                    self.getPeople(pageNumber: pageNumber + 1, people: people + response.results, completionHandler: completionHandler)
                } else {
                    completionHandler(people + response.results)
                }
            }
            .store(in: &subscriptions)
    }
    
     func getPerson(name: String, completionHandler: @escaping (Person?) -> Void) {
        (httpClient.requestObject(from: base,
                                  using: PersonClientAPI(type: .getPerson(name))) as AnyPublisher<PersonResponse, SWError>)
            .sink { completion in
                switch completion {
                case .failure:
                    completionHandler(nil)
                case .finished:
                    break
                }
            } receiveValue: { response in
                completionHandler(response.results.first)
            }
            .store(in: &subscriptions)
    }
}

