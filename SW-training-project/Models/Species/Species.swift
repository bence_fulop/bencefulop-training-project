//
//  Species.swift
//  SW-training-project
//
//  Created by Bence Fulop on 12/5/21.
//

import Foundation

struct Species: SWObject {
    var identifier: String {
        return "SpeciesTableViewCell"
    }
    var name: String?
    var classification: String?
    var designation: String?
    var average_height: String?
    var hair_colors: String?
    var eye_colors: String?
    var average_lifespan: String?
    var language: String?
}
