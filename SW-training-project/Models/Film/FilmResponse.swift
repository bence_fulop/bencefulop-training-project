//
//  FilmResponse.swift
//  SW-training-project
//
//  Created by Bence Fulop on 12/5/21.
//

import Foundation

struct FilmResponse: Codable {
    let next: String?
    let results: [Film]
}
