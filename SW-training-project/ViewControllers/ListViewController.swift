//
//  ListViewController.swift
//  SW-training-project
//
//  Created by Bence Fulop on 24/5/21.
//

import UIKit

class ListViewController: UIViewController {
    
    var resultList: [SWObject]?

    @IBAction func closeButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    
    @IBAction func homeButtonTapped(_ sender: Any) {
        let presentingViewController = self.presentingViewController
        self.dismiss(animated: true) {
            presentingViewController?.dismiss(animated: true, completion: nil)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        print(resultList!)
        print(resultList!.count)
    }
    

}
