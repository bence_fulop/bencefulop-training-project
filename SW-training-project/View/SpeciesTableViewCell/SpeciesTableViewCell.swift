//
//  SpeciesTableViewCell.swift
//  SW-training-project
//
//  Created by Bence Fulop on 17/5/21.
//

import UIKit

class SpeciesTableViewCell: UITableViewCell, SWTableViewCellType {
    func configure(object: SWObject) {
        guard let object = object as? Species else {
            return
        }
        speciesNameLabel.text = object.name
        speciesClassificationLabel.text = object.classification
        speciesDesignationLabel.text = object.designation
        speciesLanguageLabel.text = object.language
        speciesAverageHeightLabel.text = "\(object.average_height!) cm"
        speciesHairColoursLabel.text = object.hair_colors
        speciesEyeColoursLabel.text = object.eye_colors
        speciesAverageLifespanLabel.text = "\(object.average_lifespan!) years"
    }

    @IBOutlet var speciesNameLabel: UILabel!
    @IBOutlet var speciesClassificationLabel: UILabel!
    @IBOutlet var speciesDesignationLabel: UILabel!
    @IBOutlet var speciesLanguageLabel: UILabel!
    @IBOutlet var speciesAverageHeightLabel: UILabel!
    @IBOutlet var speciesHairColoursLabel: UILabel!
    @IBOutlet var speciesEyeColoursLabel: UILabel!
    @IBOutlet var speciesAverageLifespanLabel: UILabel!
}
