//
//  SWObject.swift
//  SW-training-project
//
//  Created by Bence Fulop on 12/5/21.
//

import Foundation

protocol SWObject: Codable {
    var identifier: String { get }
}
