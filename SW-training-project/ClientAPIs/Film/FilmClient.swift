////
////  FilmClient.swift
////  SW-training-project
////
////  Created by Bence Fulop on 12/5/21.
////

import Combine
import Foundation

protocol FilmClientType {
    func getFilms(pageNumber: Int, films: [Film], completionHandler: @escaping ([Film]?) -> Void)
    func getFilm(name: String, completionHandler: @escaping (Film?) -> Void)
}

class FilmClient: FilmClientType {

    private var subscriptions: Set<AnyCancellable> = []
    private let httpClient: HTTPClientType
    private let base: URL

    init() {
        httpClient = HTTPClient()
        base = URL(string: "https://swapi.dev/api")!
    }

    func getFilms(pageNumber: Int, films: [Film], completionHandler: @escaping ([Film]?) -> Void) {
       (httpClient.requestObject(from: base,
                                 using: FilmClientAPI(type: .getFilms(pageNumber))) as AnyPublisher<FilmResponse, SWError>)
           .sink { completion in
               switch completion {
               case .failure:
                   completionHandler(nil)
               case .finished:
                   break
               }
           } receiveValue: { response in
               if response.next != nil {
                   self.getFilms(pageNumber: pageNumber + 1, films: films + response.results, completionHandler: completionHandler)
               } else {
                   completionHandler(films + response.results)
               }
           }
           .store(in: &subscriptions)
   }

    func getFilm(name: String, completionHandler: @escaping (Film?) -> Void) {
        (httpClient.requestObject(from: base,
                                  using: FilmClientAPI(type: .getFilm(name))) as AnyPublisher<FilmResponse, SWError>)
            .sink { completion in
                switch completion {
                case .failure:
                    completionHandler(nil)
                case .finished:
                    break
                }
            } receiveValue: { response in
                completionHandler(response.results.first)
            }
            .store(in: &subscriptions)
    }
}
