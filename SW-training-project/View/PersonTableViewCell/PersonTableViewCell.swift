//
//  PersonTableViewCell.swift
//  SW-training-project
//
//  Created by Bence Fulop on 13/5/21.
//

import UIKit

class PersonTableViewCell: UITableViewCell, SWTableViewCellType {
    func configure(object: SWObject) {
        guard let object = object as? Person else {
            return
        }
        personNameLabel.text = object.name
        personHairColourLabel.text = object.hair_color
        personEyeColourLabel.text = object.eye_color
        personSkinColourLabel.text = object.skin_color
        personMassLabel.text = "\(object.mass!) kg"
        personHeightLabel.text = "\(object.height!) cm"
        personGenderLabel.text = object.gender
        personBirthYearLabel.text = object.birth_year
    }

    @IBOutlet private var personBirthYearLabel: UILabel!
    @IBOutlet private var personHairColourLabel: UILabel!
    @IBOutlet private var personEyeColourLabel: UILabel!
    @IBOutlet private var personSkinColourLabel: UILabel!
    @IBOutlet private var personMassLabel: UILabel!
    @IBOutlet private var personHeightLabel: UILabel!
    @IBOutlet private var personGenderLabel: UILabel!
    @IBOutlet private var personNameLabel: UILabel!
}
