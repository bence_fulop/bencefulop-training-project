//
//  StarshipClientAPI.swift
//  SW-training-project
//
//  Created by Bence Fulop on 12/5/21.
//

import Foundation
enum StarshipClientAPIType {
    case getStarships(Int)
    case getStarship(String)
}

struct StarshipClientAPI: SWAPI {
    let type: StarshipClientAPIType

    var path: String {
        switch type {
        case .getStarships:
            return "/starships/"
        case .getStarship:
            return "/starships/"
        }
    }

    var method: RequestMethod {
        switch type {
        case .getStarships, .getStarship:
            return .get
        }
    }

    var parameters: RequestParameters? {
        switch type {
        case .getStarships(let pageNumber):
            return ["page": "\(pageNumber)"]
        case .getStarship(let name):
            return ["search": name]
        }
    }

    init(type: StarshipClientAPIType) {
        self.type = type
    }
}
