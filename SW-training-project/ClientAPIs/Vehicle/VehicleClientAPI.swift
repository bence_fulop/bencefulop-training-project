//
//  VehicleClientAPI.swift
//  SW-training-project
//
//  Created by Bence Fulop on 12/5/21.
//

import Foundation

enum VehicleClientAPIType {
    case getVehicles(Int)
    case getVehicle(String)
}

struct VehicleClientAPI: SWAPI {
    let type: VehicleClientAPIType

    var path: String {
        switch type {
        case .getVehicles:
            return "/vehicles/"
        case .getVehicle:
            return "/vehicles/"
        }
    }

    var method: RequestMethod {
        switch type {
        case .getVehicles, .getVehicle:
            return .get
        }
    }

    var parameters: RequestParameters? {
        switch type {
        case .getVehicles(let pageNumber):
            return ["page": "\(pageNumber)"]
        case .getVehicle(let name):
            return ["search": name]
        }
    }

    init(type: VehicleClientAPIType) {
        self.type = type
    }
}
