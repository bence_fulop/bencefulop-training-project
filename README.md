# Star Wars Training Project V.1.0

A simple application that calls on the [Star Wars API](https://swapi.dev/) and returns details about the searched character/vehice/planet.

<gif to be inserted of working application>

## Requirements

- iOS 14.5+
- Xcode 12.5

## Installation

Download and drop ```SW-training-project.swift``` in your project.  

## Meta

Developed by Bence Fulop – [BitBucket](https://bitbucket.org/bence_fulop/) - [GitHub](https://github.com/bencefulop) - [Email](mailto:bence.fulop@arq.group)
