//
//  ResultsViewController.swift
//  SW-training-project
//
//  Created by Bence Fulop on 11/5/21.
//

import UIKit

class ResultViewController: UIViewController {

    var result: SWObject?

    @IBAction func homeButtonTapped(_ sender: Any) {
        let presentingViewController = self.presentingViewController
        self.dismiss(animated: true) {
            presentingViewController?.dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func closeButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBOutlet private var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "PersonTableViewCell", bundle: nil), forCellReuseIdentifier: "PersonTableViewCell")
        tableView.register(UINib(nibName: "PlanetTableViewCell", bundle: nil), forCellReuseIdentifier: "PlanetTableViewCell")
        tableView.register(UINib(nibName: "StarshipTableViewCell", bundle: nil), forCellReuseIdentifier: "StarshipTableViewCell")
        tableView.register(UINib(nibName: "VehicleTableViewCell", bundle: nil), forCellReuseIdentifier: "VehicleTableViewCell")
        tableView.register(UINib(nibName: "FilmTableViewCell", bundle: nil), forCellReuseIdentifier: "FilmTableViewCell")
        tableView.register(UINib(nibName: "SpeciesTableViewCell", bundle: nil), forCellReuseIdentifier: "SpeciesTableViewCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}

extension ResultViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int)  -> Int {
        (result != nil) ? 1 : 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let result = result,
              let cell = tableView.dequeueReusableCell(withIdentifier: result.identifier, for: indexPath) as? SWTableViewCellType
        else  {
            return UITableViewCell()
        }
        cell.configure(object: result)
        return cell
    }
}
