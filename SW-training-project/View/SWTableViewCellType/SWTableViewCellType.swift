//
//  File.swift
//  SW-training-project
//
//  Created by Bence Fulop on 14/5/21.
//

import UIKit

protocol SWTableViewCellType: UITableViewCell {
    func configure(object: SWObject)
}
