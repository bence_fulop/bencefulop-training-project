//
//  SWAPI.swift
//  LearningARQ
//
//  Created by Aurelien Debarge on 5/5/21.
//

import Foundation

public typealias RequestHeader = [String: String]
public typealias RequestParameters = [String: String]

public enum RequestMethod: String {
    case get = "GET"
}

protocol SWAPI {
    var path: String { get }
    var method: RequestMethod { get }
    var header: RequestHeader? { get }
    var parameters: RequestParameters? { get }
}

extension SWAPI {
    var header: RequestHeader? {
        var headers = [String: String]()
        headers["Content-Type"] = "application/json"
        return headers
    }
}
