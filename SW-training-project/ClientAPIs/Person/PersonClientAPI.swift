//
//  CharacterClientAPI.swift
//  SW-training-project
//
//  Created by Bence Fulop on 12/5/21.
//

import Foundation

enum PersonClientAPIType {
    case getPeople(Int)
    case getPerson(String)
}

struct PersonClientAPI: SWAPI {
    let type: PersonClientAPIType

    var path: String {
        switch type {
        case .getPeople:
            return "/people/"
        case .getPerson:
            return "/people/"
        }
    }

    var method: RequestMethod {
        switch type {
        case .getPeople, .getPerson:
            return .get
        }
    }

    var parameters: RequestParameters? {
        switch type {
        case .getPeople(let pageNumber):
            return ["page": "\(pageNumber)"]
        case .getPerson(let name):
            return ["search": name]
        }
    }

    init(type: PersonClientAPIType) {
        self.type = type
    }
}
