//
//  SpeciesResponse.swift
//  SW-training-project
//
//  Created by Bence Fulop on 12/5/21.
//

import Foundation

struct SpeciesResponse: Codable {
    let next: String?
    let results: [Species]
}
