//
//  PlanetTableViewCell.swift
//  SW-training-project
//
//  Created by Bence Fulop on 14/5/21.
//

import UIKit

class PlanetTableViewCell: UITableViewCell, SWTableViewCellType {
    func configure(object: SWObject) {
        guard let object = object as? Planet else {
            return
        }

        planetNameLabel.text = object.name
        planetClimateLabel.text = object.climate
        planetTerrainLabel.text = object.terrain
        planetPopulationLabel.text = object.population
        planetGravityLabel.text = object.gravity
        planetDiameterLabel.text = "\(object.diameter!) km"
        planetOrbitalPeriodLabel.text = "\(object.orbital_period!) days"
        planetRotationPeriodLabel.text = "\(object.rotation_period!) hours"

    }

    @IBOutlet private var planetNameLabel: UILabel!
    @IBOutlet private var planetClimateLabel: UILabel!
    @IBOutlet var planetPopulationLabel: UILabel!
    @IBOutlet var planetTerrainLabel: UILabel!
    @IBOutlet var planetGravityLabel: UILabel!
    @IBOutlet var planetDiameterLabel: UILabel!
    @IBOutlet var planetOrbitalPeriodLabel: UILabel!
    @IBOutlet var planetRotationPeriodLabel: UILabel!

    
}
