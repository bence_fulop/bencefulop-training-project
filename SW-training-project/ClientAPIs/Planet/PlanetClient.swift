//
//  PlanetClient.swift
//  SW-training-project
//
//  Created by Bence Fulop on 12/5/21.
//

import Combine
import Foundation

protocol PlanetClientType {
    func getPlanets(pageNumber: Int, planets: [Planet], completionHandler: @escaping ([Planet]?) -> Void)
    func getPlanet(name: String, completionHandler: @escaping (Planet?) -> Void)
}

class PlanetClient: PlanetClientType {

    private var subscriptions: Set<AnyCancellable> = []
    private let httpClient: HTTPClientType
    private let base: URL

    init() {
        httpClient = HTTPClient()
        base = URL(string: "https://swapi.dev/api")!
    }

    func getPlanets(pageNumber: Int, planets: [Planet], completionHandler: @escaping ([Planet]?) -> Void) {
       (httpClient.requestObject(from: base,
                                 using: PlanetClientAPI(type: .getPlanets(pageNumber))) as AnyPublisher<PlanetResponse, SWError>)
           .sink { completion in
               switch completion {
               case .failure:
                   completionHandler(nil)
               case .finished:
                   break
               }
           } receiveValue: { response in
               if response.next != nil {
                   self.getPlanets(pageNumber: pageNumber + 1, planets: planets + response.results, completionHandler: completionHandler)
               } else {
                   completionHandler(planets + response.results)
               }
           }
           .store(in: &subscriptions)
   }

    func getPlanet(name: String, completionHandler: @escaping (Planet?) -> Void) {
        (httpClient.requestObject(from: base,
                                  using: PlanetClientAPI(type: .getPlanet(name))) as AnyPublisher<PlanetResponse, SWError>)
            .sink { completion in
                switch completion {
                case .failure:
                    completionHandler(nil)
                case .finished:
                    break
                }
            } receiveValue: { response in
                completionHandler(response.results.first)
            }
            .store(in: &subscriptions)
    }
}


