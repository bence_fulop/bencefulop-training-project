//
//  FilmTableViewCell.swift
//  SW-training-project
//
//  Created by Bence Fulop on 17/5/21.
//

import UIKit

class FilmTableViewCell: UITableViewCell, SWTableViewCellType {
    func configure(object: SWObject) {
        guard let object = object as? Film else {
            return
        }

        filmTitleLabel.text = object.title
        filmEpisodeIdLabel.text = "\(object.episode_id!)"
        filmOpeningCrawlLabel.text = object.opening_crawl
        filmDirectorLabel.text = object.director
        filmProducerLabel.text = object.producer
        filmReleaseDateLabel.text = object.release_date
    }
    @IBOutlet var filmTitleLabel: UILabel!
    @IBOutlet var filmEpisodeIdLabel: UILabel!
    @IBOutlet var filmOpeningCrawlLabel: UILabel!
    @IBOutlet var filmDirectorLabel: UILabel!
    @IBOutlet var filmProducerLabel: UILabel!
    @IBOutlet var filmReleaseDateLabel: UILabel!


}
