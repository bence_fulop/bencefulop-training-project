//
//  SpeciesClient.swift
//  SW-training-project
//
//  Created by Bence Fulop on 12/5/21.
//

import Combine
import Foundation

protocol SpeciesClientType {
    func getSpecies(pageNumber: Int, species: [Species], completionHandler: @escaping ([Species]?) -> Void)
    func getSpecie(name: String, completionHandler: @escaping (Species?) -> Void)
}

class SpeciesClient: SpeciesClientType {

    private var subscriptions: Set<AnyCancellable> = []
    private let httpClient: HTTPClientType
    private let base: URL

    init() {
        httpClient = HTTPClient()
        base = URL(string: "https://swapi.dev/api")!
    }

    func getSpecies(pageNumber: Int, species: [Species], completionHandler: @escaping ([Species]?) -> Void) {
       (httpClient.requestObject(from: base,
                                 using: SpeciesClientAPI(type: .getSpecies(pageNumber))) as AnyPublisher<SpeciesResponse, SWError>)
           .sink { completion in
               switch completion {
               case .failure:
                   completionHandler(nil)
               case .finished:
                   break
               }
           } receiveValue: { response in
               if response.next != nil {
                   self.getSpecies(pageNumber: pageNumber + 1, species: species + response.results, completionHandler: completionHandler)
               } else {
                   completionHandler(species + response.results)
               }
           }
           .store(in: &subscriptions)
   }

    func getSpecie(name: String, completionHandler: @escaping (Species?) -> Void) {
        (httpClient.requestObject(from: base,
                                  using: SpeciesClientAPI(type: .getSpecie(name))) as AnyPublisher<SpeciesResponse, SWError>)
            .sink { completion in
                switch completion {
                case .failure:
                    completionHandler(nil)
                case .finished:
                    break
                }
            } receiveValue: { response in
                completionHandler(response.results.first)
            }
            .store(in: &subscriptions)
    }
}



