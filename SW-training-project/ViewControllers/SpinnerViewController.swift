//
//  SpinnerViewController.swift
//  SW-training-project
//
//  Created by Bence Fulop on 13/5/21.
//

import UIKit

class SpinnerViewController: UIViewController {
    var spinner = UIActivityIndicatorView(style: .large)

    override func loadView() {
        view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.7)

        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.color = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        spinner.startAnimating()
        view.addSubview(spinner)

        spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}

func createSpinnerView(_ parent: UIViewController) {
    // spinner for activity indication
    let child = SpinnerViewController()

    parent.addChild(child)
    child.view.frame = parent.view.frame
    parent.view.addSubview(child.view)
    child.didMove(toParent: parent)

    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
}
