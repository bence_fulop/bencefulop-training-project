//
//  VehicleClient.swift
//  SW-training-project
//
//  Created by Bence Fulop on 12/5/21.
//

import Combine
import Foundation

protocol VehicleClientType {
    func getVehicles(pageNumber: Int, vehicles: [Vehicle], completionHandler: @escaping ([Vehicle]?) -> Void)
    func getVehicle(name: String, completionHandler: @escaping (Vehicle?) -> Void)
}

class VehicleClient: VehicleClientType {

    private var subscriptions: Set<AnyCancellable> = []
    private let httpClient: HTTPClientType
    private let base: URL

    init() {
        httpClient = HTTPClient()
        base = URL(string: "https://swapi.dev/api")!
    }

    func getVehicles(pageNumber: Int, vehicles: [Vehicle], completionHandler: @escaping ([Vehicle]?) -> Void) {
       (httpClient.requestObject(from: base,
                                 using: VehicleClientAPI(type: .getVehicles(pageNumber))) as AnyPublisher<VehicleResponse, SWError>)
           .sink { completion in
               switch completion {
               case .failure:
                   completionHandler(nil)
               case .finished:
                   break
               }
           } receiveValue: { response in
               if response.next != nil {
                   self.getVehicles(pageNumber: pageNumber + 1, vehicles: vehicles + response.results, completionHandler: completionHandler)
               } else {
                   completionHandler(vehicles + response.results)
               }
           }
           .store(in: &subscriptions)
   }

    func getVehicle(name: String, completionHandler: @escaping (Vehicle?) -> Void) {
        (httpClient.requestObject(from: base,
                                  using: VehicleClientAPI(type: .getVehicle(name))) as AnyPublisher<VehicleResponse, SWError>)
            .sink { completion in
                switch completion {
                case .failure:
                    completionHandler(nil)
                case .finished:
                    break
                }
            } receiveValue: { response in
                completionHandler(response.results.first)
            }
            .store(in: &subscriptions)
    }
}




