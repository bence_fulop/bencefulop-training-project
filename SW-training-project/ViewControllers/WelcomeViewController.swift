//
//  WelcomeViewController.swift
//  SW-training-project
//
//  Created by Bence Fulop on 18/5/21.
//

import UIKit
import AVFoundation

class WelcomeViewController: UIViewController {
    @IBOutlet var SWLogo: UIImageView!
    @IBOutlet var browseArchivesButton: UIButton!
    @IBOutlet var sourceCodeButton: UIButton!
    @IBOutlet var videoLayer: UIView!
    @IBOutlet var buttonStack: UIStackView!

    var playerLooper: NSObject?
    var playerLayer:AVPlayerLayer!
    var queuePlayer: AVQueuePlayer?



    override func viewDidLoad() {
        super.viewDidLoad()
        playVideo()
        browseArchivesButton.layer.cornerRadius = 10
        browseArchivesButton.clipsToBounds = true
        sourceCodeButton.layer.cornerRadius = 10
        sourceCodeButton.clipsToBounds = true
        sourceCodeButton.layer.borderWidth = 1
        sourceCodeButton.layer.borderColor = #colorLiteral(red: 0.9960784314, green: 0.768627451, blue: 0, alpha: 1).cgColor
    }

    @IBAction func sourceCodeButtonTapped(_ sender: Any) {
        guard let url = URL(string: "https://bitbucket.org/bence_fulop/bencefulop-training-project/src/main/") else { return }
        UIApplication.shared.open(url)
    }
    @IBAction func browseArchivesButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "goToMain", sender: self)
        stopVideo()
    }

    func playVideo() {
        guard let path = Bundle.main.path(forResource: "stars_short", ofType: "mp4") else {
            return
        }
        let url =  URL(fileURLWithPath: path)
        let playerItem = AVPlayerItem(url: url as URL)
        self.queuePlayer = AVQueuePlayer(items: [playerItem])
        self.playerLayer = AVPlayerLayer(player: self.queuePlayer)
        self.playerLooper = AVPlayerLooper(player: self.queuePlayer!, templateItem: playerItem)
        self.videoLayer.layer.addSublayer(playerLayer)
        self.playerLayer?.frame = self.view.frame
        self.playerLayer.videoGravity = .resizeAspectFill
        self.videoLayer.bringSubviewToFront(SWLogo)
        self.videoLayer.bringSubviewToFront(browseArchivesButton)
        self.videoLayer.bringSubviewToFront(sourceCodeButton)
        self.videoLayer.bringSubviewToFront(buttonStack)

        self.queuePlayer?.play()

    }

    func stopVideo() {
        self.queuePlayer!.replaceCurrentItem(with: nil)
    }

}

