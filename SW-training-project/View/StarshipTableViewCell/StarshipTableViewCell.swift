//
//  StarshipTableViewCell.swift
//  SW-training-project
//
//  Created by Bence Fulop on 14/5/21.
//

import UIKit

class StarshipTableViewCell: UITableViewCell, SWTableViewCellType {
    func configure(object: SWObject) {
        guard let object = object as? Starship else {
            return
        }

        starshipNameLabel.text = object.name
        starshipModelLabel.text = object.model
        starshipMaxAtmosphereSpeedLabel.text = "\(object.max_atmosphering_speed!) km/h"
        starshipHyperdriveRatingLabel.text = object.hyperdrive_rating
        starshipCrewLabel.text = object.crew
        starshipLengthLabel.text = "\(object.length!) m"
        starshipCostInCreditsLabel.text = object.cost_in_credits
        starshipManufacturerLabel.text = object.manufacturer
    }

    @IBOutlet private var starshipNameLabel: UILabel!
    @IBOutlet private var starshipModelLabel: UILabel!
    @IBOutlet private var starshipMaxAtmosphereSpeedLabel: UILabel!
    @IBOutlet private var starshipHyperdriveRatingLabel: UILabel!
    @IBOutlet private var starshipCrewLabel: UILabel!
    @IBOutlet private var starshipLengthLabel: UILabel!
    @IBOutlet private var starshipCostInCreditsLabel: UILabel!
    @IBOutlet private var starshipManufacturerLabel: UILabel!
}

