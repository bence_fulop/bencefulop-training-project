//
//  HTTPClient.swift
//  LearningARQ
//
//  Created by Aurelien Debarge on 5/5/21.
//

import Combine
import Foundation

enum SWError: Error {
    case decodeFail(Error)
    case unknown(Error?)
}

protocol HTTPClientType {

    func requestObject<Object: Decodable>(from url: URL,
                                          using api: SWAPI) -> AnyPublisher<Object, SWError>
}

struct HTTPClient: HTTPClientType {
    private let session: URLSession

    public init(session: URLSession = URLSession.shared) {
        self.session = session
    }

    func requestObject<Object: Decodable>(from url: URL,
                                          using api: SWAPI) -> AnyPublisher<Object, SWError> {
        let request = self.request(from: url, using: api)

        return session.dataTaskPublisher(for: request)
            .tryMap { response in
                guard let httpURLResponse = response.response as? HTTPURLResponse,
                  httpURLResponse.statusCode == 200 else {
                    throw SWError.unknown(nil)
                }
                return response.data
            }
            .decode(type: Object.self, decoder: JSONDecoder())
            .mapError(SWError.decodeFail)
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }

    private func request(from url: URL, using api: SWAPI) -> URLRequest {
        let url = url.appendingPathComponent(api.path)
        var components = URLComponents(string: url.absoluteString)!

        if let parameters = api.parameters {
            components.queryItems = parameters.map { (key, value) in
                URLQueryItem(name: key, value: value)
            }
        }
        var request = URLRequest(url: components.url!)
        request.httpMethod = api.method.rawValue
        request.allHTTPHeaderFields = api.header

        return request
    }
}
