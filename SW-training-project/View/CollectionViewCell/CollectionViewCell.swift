//
//  CollectionViewCell.swift
//  SW-training-project
//
//  Created by Bence Fulop on 20/5/21.
//

import UIKit
import FontAwesome_swift

var fontAwesomeIcons: [String:FontAwesome] = [
    "People" : FontAwesome.userAlt,
    "Planets" : FontAwesome.globeEurope,
    "Starships" : FontAwesome.rocket,
    "Vehicles" :  FontAwesome.truckMonster,
    "Species" : FontAwesome.spider,
    "Films" : FontAwesome.ticketAlt,
]

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet var nameLabel: UILabel!

    
    @IBOutlet weak var cellIcon: UIImageView!
    class var reuseIdentifier: String {
        return "CollectionViewCellReuseIdentifier"
    }
    class var nibName: String {
        return "CollectionViewCell"
    }

    func configureCell(name: String) {
        self.nameLabel.text = name
        self.cellIcon.image = UIImage.fontAwesomeIcon(name: fontAwesomeIcons[name] ?? .userAlt, style: .solid, textColor: .white, size: CGSize(width: 121, height: 74 ))
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
