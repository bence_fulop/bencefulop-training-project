//
//  Starship.swift
//  SW-training-project
//
//  Created by Bence Fulop on 12/5/21.
//

import Foundation

struct Starship: SWObject {
    var identifier: String {
        return "StarshipTableViewCell"
    }
    var name: String?
    var model: String?
    var manufacturer: String?
    var cost_in_credits: String?
    var length: String?
    var max_atmosphering_speed: String?
    var crew: String?
    var hyperdrive_rating: String?
}
