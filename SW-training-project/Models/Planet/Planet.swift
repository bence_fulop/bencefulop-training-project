//
//  Planet.swift
//  SW-training-project
//
//  Created by Bence Fulop on 12/5/21.
//

import Foundation

struct Planet: SWObject {
    var identifier: String {
        return "PlanetTableViewCell"
    }
    var name: String?
    var rotation_period: String?
    var orbital_period: String?
    var diameter: String?
    var climate: String?
    var gravity: String?
    var terrain: String?
    var population: String?
}
